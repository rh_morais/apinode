
exports.post = (req, res, next) => {
    res.status(201).send('Post recebido com sucesso!');
};

exports.put = (req, res, next) => {
    let id = req.params.id;
    res.status(201).send(`Put recebido com sucesso para ${id}`);
};

exports.delete = (req, res, next) => {
    let id = req.params.id;
    res.status(200).send(`Delete recebido com sucesso para ${id}`);
};

exports.get = (req, res, next) => {
    let id = req.params.id;
    res.status(200).send(`Get recebido com sucesso para ${id}`);
};